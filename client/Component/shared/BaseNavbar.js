import React from "react"
import Link from "next/link"
import { motion } from "framer-motion"

const BaseNavbar = props => {
  return (
    <motion.nav
      animate={{ y: 0, opacity: 1 }}
      initial={{ y: -72, opacity: 0 }}
      transition={{
        duration: 1,
        ease: [0.6, 0.05, -0.01, 0.9],
      }}
      style={{ position: 'absolute' }}
    >
      <div className="container">
        <Link href="/">
          <img className="logo" src="/static/images/logo.png" />
          {/* <span>Fitslanguage</span> */}
        </Link>

        <ul className="page-list">
          <li className="active">
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>How it works</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Prices</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Offline Classes</a>
            </Link>
          </li>
          <li className="login">
            <Link href="/login">
              <a>Log in</a>
            </Link>
          </li>
          <Link href="/register">
            <li className="signup">
              <a>Sign up</a>
            </li>
          </Link>
        </ul>
      </div>
    </motion.nav>
  )
}
export default BaseNavbar
