import React from "react"
import Head from "next/head"
import BaseNavbar from "../shared/BaseNavbar"
import { motion } from "framer-motion"
import styled from 'styled-components'

const BaseLayout = ({ children, titlePage }) => {
  //   console.log('BaseLayout state: ', state);
  return (
    <>
      <Head>
        <title>{titlePage}</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta
          name="description"
          content="Learn to speak English online conveniently and effectively with our private lessons via Skype or Hangouts."
        />
        <meta name="robots" content="index,follow" />

        {/* <!-- OpenGraph metadata--> */}
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="FitsLanguage - Online English Classes"
        />
        <meta
          property="og:description"
          content="Learn to speak English online conveniently and effectively with our private lessons via Skype or Hangouts."
        />
        <meta property="og:url" content="https://www.fitslanguage.com/" />
        <meta property="og:site_name" content="FitsLanguage" />
        {/* <meta property="og:image" content="http://mydomain.com/image.png" /> */}
        <meta property="fb:admins" content="2295447523999979" />
        <meta name="twitter:card" content="summary" />
        <meta
          name="twitter:description"
          content="Learn to speak English online conveniently and effectively with our private lessons via Skype or Hangouts."
        />
        <meta
          name="twitter:title"
          content="FitsLanguage - Online English Classes"
        />
        <meta name="twitter:site" content="@fits_language" />
        <meta name="twitter:creator" content="@fits_language" />
        {/* <!-- Pop up --> */}

        <link
          rel="apple-touch-icon"
          sizes="57x57"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="60x60"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-60x60.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="76x76"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="114x114"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="120x120"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="152x152"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-152x152.png"
        />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="https://www.fitslanguage.com/images/favicon/apple-icon-180x180.png"
        />

        <link
          rel="icon"
          type="image/png"
          sizes="192x192"
          href="https://www.fitslanguage.com/images/favicon/android-icon-192x192.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="https://www.fitslanguage.com/images/favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="96x96"
          href="https://www.fitslanguage.com/images/favicon/favicon-96x96.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="https://www.fitslanguage.com/images/favicon/favicon-16x16.png"
        />

        <link
          rel="manifest"
          href="https://www.fitslanguage.com/images/favicon/manifest.json"
        />
        <meta name="msapplication-TileColor" content="#2185D0" />
        <meta
          name="msapplication-TileImage"
          content="https://www.fitslanguage.com/images/favicon/ms-icon-144x144.png"
        />
        <meta name="theme-color" content="#2185D0" />
      </Head>
      <BaseNavbar />
      <Container initial="initial" animate="animate" exit={{ opacity: 0 }}>
        {children}
      </Container>
    </>
  )
}

// function mapStateToProps(state) {
//   return { state: state };
// }
// const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(BaseLayout);

export default BaseLayout



const Container = styled(motion.div)
  `
    padding-top: 110px;
    width: 1100px;
    margin: auto;
  `
