import Router from 'next/router'

export default (context, target) => {
    if (context.res) {
        context.res.writeHead(303, { Location: target })
    } else {
        Router.replace(target)
    }
}