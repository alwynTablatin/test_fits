import React from 'react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import BaseLayout from '../Component/layouts/BaseLayout';
import { Button, TextField, } from '@material-ui/core';
import axios from 'axios'
import withAuth from '../hooks/withAuth'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { saveUser } from '../Redux/slices';
import { useDispatch } from 'react-redux';

const Login = (props) => {
    const dispatch = useDispatch()

    const validationSchema = Yup.object({
        email: Yup.string()
            .email('Invalid email address')
            .required('Required'),
        password: Yup.string().required('Required')
    })

    const initialValues = {
        firstName: '',
        lastName: '',
        email: '',
        password: ''
    }

    const loginUser = (values) => {
        axios
            .post('/api/v1/auth/login', values, { 'Content-Type': 'application/json' })
            .then((res) => {
                console.log('response: ', res.data)
                if (res.data.role) {
                    dispatch(saveUser({ role: res.data.role, token: res.data.token }))
                }
            })
            .catch((error) => {
                // props.setErrors(error.response.data.error)
                console.log('error: ', error.response.data);
            });
    }


    return (
        <BaseLayout titlePage="FitsLanguage - Online English Classes | Log in">
            <Formik
                validationSchema={validationSchema}
                initialValues={initialValues}
                onSubmit={(values, formikHelpers) => {
                    return new Promise((next) => {
                        loginUser(values)
                        next();
                    });
                }}
            >
                {({ values, errors, isSubmitting, isValidating, touched }) => (
                    <Form>
                        <Field as={TextField} error={touched.email && errors.email ? true : false} helperText={touched.email && errors.email ? errors.email : null} name="email" label="Email Address" variant="outlined" type="email" />
                        <Field as={TextField} error={touched.password && errors.password ? true : false} helperText={touched.password && errors.password ? errors.password : null} name="password" label="Password" variant="outlined" type="password" />
                        <Button type="submit" variant="contained" color="primary" disabled={isSubmitting || isValidating}>Submit</Button>
                        <pre>{JSON.stringify(errors, null, 4)}</pre>
                        <pre>{JSON.stringify(values, null, 4)}</pre>
                    </Form>
                )}
            </Formik>
        </BaseLayout>
    );
};
function mapStateToProps(state) {
    return state;
}

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withAuth('Guest')(Login));