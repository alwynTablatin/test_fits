import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import {
//   sample,
// } from '../Redux/actions/authActions';
import BaseLayout from '../../Component/layouts/BaseLayout';

import withAuth from '../../hooks/withAuth'

const Index = (props) => {
    console.log('index student: ', props)
    return (
        <BaseLayout titlePage="FitsLanguage - Online English Classes | Home">
            <h1>home student page</h1>
        </BaseLayout>
    );
};

Index.getInitialProps = async ({ reduxStore }) => {
    //   const isServer = !!req;
    // console.log('index getInitialProps: ', reduxStore.dispatch(sample()));
    return { initialProps: 'sample' }
};

function mapStateToProps(state) {
    return state;
}
const mapDispatchToProps = (dispatch) =>
    bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withAuth('Student')(Index));
