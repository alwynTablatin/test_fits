import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BaseLayout from '../Component/layouts/BaseLayout';
import styled from 'styled-components'

const Index = (props) => {
  return (
    <BaseLayout titlePage="FitsLanguage - Online English Classes | Home">
      <Banner>
        <Title>
          <Yellowtext>
            TAKE YOUR FIRST STEP
          </Yellowtext>
          <br />
          TO YOUR ENGLISH FLUENCY
        </Title>
        <br />
          Try it for free <img src="/static/images/arrow-right.svg" />
      </Banner>
    </BaseLayout>
  );
};

Index.getInitialProps = async ({ reduxStore }) => {
  // console.log('index getInitialProps: ', reduxStore.dispatch(sample()));
};

function mapStateToProps(state) {
  return state;
}
const mapDispatchToProps = (dispatch) =>
  bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Index);

// styled compoenents below

const Banner = styled.div
  `
  width:100%;
  background-color:white;
  height:500px;
  background: url(/static/images/readingBook.png);
  background-position: 95% 100%;
  background-repeat: no-repeat;
  background-size: contain;
`

const Title = styled.span
  `
  font-size: 55px;
  width: 45%;
  display: block;
  top: 0;
  left: 0;
  line-height: 55px;
  text-align: left;
  padding-top: 200px;
  padding-left: 100px;
  font-weight: bolder;
`

const Yellowtext = styled.span
  `
color:#ECBE1C;
`