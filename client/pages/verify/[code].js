import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios'
import useSWR from 'swr'
import Router from 'next/router'

const Verify = (props) => {
    const { data, error } = useSWR(`/api/v1/auth/verify/${props.verificationCode}`, (url) => axios.put(url).then(res => res.data))
    console.log(data)

    if (error) {
        return <h1>Your verificationCode is invalid</h1>
    }
    if (data) {
        Router.push(`/${data.role.toLowerCase()}`)
    }
    return (
        <h1>Verifying data..... please wait</h1>
    );
};

Verify.getInitialProps = async (ctx) => {
    //   const isServer = !!req;
    // console.log('Verify getInitialProps: ', ctx.req.headers.host, ctx.query.code);
    return { verificationCode: ctx.query.code }
};

function mapStateToProps(state) {
    return state;
}
const mapDispatchToProps = (dispatch) =>
    bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Verify);
