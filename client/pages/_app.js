// import App from "next/app"
import React from "react"
import withReduxStore from "../lib/with-redux-store"
import { Provider } from "react-redux"
import { persistStore } from "redux-persist"
import { PersistGate } from "redux-persist/integration/react"
import { AnimatePresence } from "framer-motion"
import { SWRConfig } from 'swr'
import "../styles/main.scss"
import axios from "axios"

// class MyApp extends App {
//   constructor(props) {
//     super(props)
//     this.persistor = persistStore(props.reduxStore)
//   }

//   static async getInitialProps({ Component, router, ctx }) {
//     // console.log('router: ', router);
//     // console.log('Component: ', Component);
//     // console.log('ctx: ', ctx)

//     let pageProps = {}
//     if (Component.getInitialProps) {
//       pageProps = await Component.getInitialProps(ctx)
//     }

//     return { pageProps }
//   }

//   render() {
//     // console.log('MyApp props: ', this.props)
//     const { Component, pageProps, reduxStore } = this.props
//     return (
//       <AnimatePresence exitBeforeEnter>
//         <Provider store={reduxStore}>
//           <PersistGate loading={null} persistor={this.persistor}>
//             <SWRConfig value={{ fetcher: (url) => axios(url).then(res => res.data) }}>
//               <Component {...pageProps} />
//             </SWRConfig>
//           </PersistGate>
//         </Provider>
//       </AnimatePresence>
//     )
//   }
// }

// export default withReduxStore(MyApp)

const App = ({ Component, pageProps, reduxStore }) => {
  // console.log('reduxStore: ', reduxStore)
  return (
    <AnimatePresence exitBeforeEnter>
      <Provider store={reduxStore}>
        <PersistGate loading={null} persistor={persistStore(reduxStore)}>
          <SWRConfig value={{ fetcher: (url) => axios(url).then(res => res.data) }}>
            <Component {...pageProps} />
          </SWRConfig>
        </PersistGate>
      </Provider>
    </AnimatePresence>
  )
  // return <Component {...pageProps} />
}

export default withReduxStore(App)