// import React from 'react';
import { Field, Form, Formik } from 'formik';
import * as Yup from 'yup';
import BaseLayout from '../Component/layouts/BaseLayout';
import {
  Button,
  TextField,
} from '@material-ui/core';
import axios from 'axios'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useRouter } from 'next/router';
import withAuth from '../hooks/withAuth'
import { saveUser } from '../Redux/slices';
import { useDispatch, useSelector } from 'react-redux';


const Register = (props) => {
  const dispatch = useDispatch();

  const validationSchema = Yup.object({
    firstName: Yup.string()
      .max(15, 'Must be 15 characters or less')
      .required('Required'),
    lastName: Yup.string()
      .max(20, 'Must be 20 characters or less')
      .required('Required'),
    email: Yup.string()
      .email('Invalid email address')
      .required('Required'),
    password: Yup.string().required('Required')
  })

  const initialValues = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  }

  const registerUser = (values) => {
    axios
      .post('/api/v1/auth/register', values, { 'Content-Type': 'application/json' })
      .then((res) => {
        console.log('response: ', res.data)
        if (res.data.role) {
          dispatch(saveUser({ role: res.data.role, token: res.data.token }))
        }
      })
      .catch((error) => {
        // props.setErrors(error.response.data.error)
        console.log('error: ', error.response.data);
      });
  }


  return (
    <BaseLayout titlePage="FitsLanguage - Online English Classes | Register">
      <Formik
        validationSchema={validationSchema}
        initialValues={initialValues}
        onSubmit={(values, formikHelpers) => {
          registerUser(values)
        }}
      >
        {({ values, errors, isSubmitting, isValidating, touched }) => (
          <Form>
            <Field as={TextField} name="firstName" error={touched.firstName && errors.firstName ? true : false} helperText={touched.firstName && errors.firstName ? errors.firstName : null} label="First Name" variant="outlined" type="text" />
            <Field as={TextField} error={touched.lastName && errors.lastName ? true : false} helperText={touched.lastName && errors.lastName ? errors.lastName : null} name="lastName" label="Last Name" variant="outlined" type="text" />
            <Field as={TextField} error={touched.email && errors.email ? true : false} helperText={touched.email && errors.email ? errors.email : null} name="email" label="Email Address" variant="outlined" type="email" />
            <Field as={TextField} error={touched.password && errors.password ? true : false} helperText={touched.password && errors.password ? errors.password : null} name="password" label="Password" variant="outlined" type="password" />
            <Button type="submit" variant="contained" color="primary" disabled={isSubmitting || isValidating}>Submit</Button>
            <pre>{JSON.stringify(errors, null, 4)}</pre>
            <pre>{JSON.stringify(values, null, 4)}</pre>
          </Form>
        )}
      </Formik>
    </BaseLayout>
  );
};

// export const getServerSideProps = async (ctx) => {
//   return { props: { sample: 'oasdiashj' } }
// }

function mapStateToProps(state) {
  return state;
}

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withAuth('Guest')(Register));
