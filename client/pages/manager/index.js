import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
// import {
//   sample,
// } from '../Redux/actions/authActions';
import BaseLayout from '../../Component/layouts/BaseLayout';
import withAuth from '../../hooks/withAuth'

const Index = (props) => {
    // const increment = () => {
    //   const { incrementCount } = props;
    //   incrementCount();
    // };

    // const decrement = () => {
    //   const { decrementCount } = props;
    //   decrementCount();
    // };

    // const reset = () => {
    //   const { resetCount } = props;
    //   resetCount();
    // };

    // const { count } = props.auth;
    return (
        <BaseLayout titlePage="FitsLanguage - Online English Classes | Home">
            <h1>home manager page
      </h1>
        </BaseLayout>
    );
};

Index.getInitialProps = async ({ reduxStore }) => {
    //   const isServer = !!req;
    // console.log('index getInitialProps: ', reduxStore.dispatch(sample()));
};

function mapStateToProps(state) {
    return state;
}
const mapDispatchToProps = (dispatch) =>
    bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withAuth('Manager')(Index));
