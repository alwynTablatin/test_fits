import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {
  configureStore,
  combineReducers,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';

import { userReducer } from './slices';

const rootReducer = combineReducers({
  user: userReducer,
});


const persistConfig = {
  key: 'primary',
  storage,
  //   whitelist: ['auth'], // place to select which state you want to persist
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export function initializeStore(initialState = {}) {
  return configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware({
      serializableCheck: false,
    }),
  }
  );
}