import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    userRole: 'Guest',
    token: ''
};

const { actions, reducer } = createSlice({
    name: 'user',
    initialState,
    reducers: {
        save: (state, { payload }) => {
            state.userRole = payload.role;
            state.token = payload.token;
        },
        remove: () => initialState,
    },
});

export const getUserRole = (state) => state.user.userRole;

export const {
    save: saveUser,
    remove: removeUser,
} = actions;

export { reducer as userReducer };
