import React from 'react';
import Router from 'next/router'
import { removeUser, getUserRole } from '../Redux/slices';
import { useDispatch, useSelector } from 'react-redux';

export default permissionRole => Component => pageProps => {
    const dispatch = useDispatch()
    const currentUserRole = useSelector(getUserRole)

    console.log('withAuth pageProps: ', pageProps)

    const isAuthenticated = () => {

        const { user } = pageProps
        if (user?.userRole || currentUserRole) {
            if (user?.userRole == permissionRole || currentUserRole == permissionRole) {
                return <Component {...pageProps} />
            } else {
                if (user?.userRole !== 'Guest' || currentUserRole !== 'Guest') {
                    Router.push(`/${user.userRole.toLowerCase()}`)
                } else {
                    dispatch(removeUser())
                    Router.push(`/login`)
                }
                return <h1>Redirecting....</h1>
            }
        } else {
            dispatch(removeUser())
        }

    }

    return isAuthenticated()
}


// class withAuth extends React.Component {

//     // static async getInitialProps(args) {
//     //     // console.log('withAuth router: ', args);
//     //     const pageProps =
//     //         (await Component.getInitialProps) &&
//     //         (await Component.getInitialProps(args));

//     //     return { ...pageProps };
//     // }

//     isAuthenticated() {
//         // console.log('withAuth props: ', this.props, Router)
//         // const { back, router, push } = Router
//         // axios.get('/api/v1/auth/me', { 'Content-Type': 'application/json' })
//         //     .then(response => {
//         //         if (response.status == 200) {
//         //             const { role } = response.data.data
//         //             if (router.pathname == '/login' || router.pathname == '/register') {
//         //                 push(`/${role.toLowerCase()}`)
//         //             } else if (permissionRole != role) {
//         //                 back()
//         //             }
//         //         }
//         //     }).catch(err => {
//         //         // console.log("error: ", err.response)
//         //         if (permissionRole != 'Guest') {
//         //             if (err.response.data.error == 'Not authorized to access this route') {
//         //                 push('/login')
//         //             } else {
//         //                 push('/')
//         //             }
//         //         }
//         //     })

//         return <Component {...this.props} />
//     }

//     render() {
//         console.log('withAuth props: ', this.props)
//         return this.isAuthenticated()
//     }
// }