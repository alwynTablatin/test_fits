const express = require('express');

const {
  register,
  login,
  logout,
  getMe,
  updateDetails,
  updatePassword,
  verifyAccount
} = require('../controller/auth');

const router = express.Router({ mergeParams: true });

const { protect } = require('../middleware/auth');

router.post('/register', register);

router.post('/login', login);

router.get('/logout', protect, logout);

router.get('/me', protect, getMe);

router.get('/updatedetails', protect, updateDetails);

router.get('/updatepassword', protect, updatePassword);

router.put('/verify/:verificationCode', verifyAccount);

module.exports = router;
