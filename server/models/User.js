const crypto = require('crypto');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
  firstName: {
    type: String,
    // required: [true, 'Please add a first name'],
  },
  lastName: {
    type: String,
    // required: [true, 'Please add a last name'],
  },
  avatar: {
    type: String,
    default: 'no-photo.jpg',
  },
  currency: {
    type: String,
  },
  country: {
    type: String,
  },
  region: {
    type: String,
  },
  age: {
    type: String,
  },
  langauge: {
    type: String,
  },
  contact: {
    type: String,
    enum: ['Skype', 'Hangout'],
    default: 'Skype',
  },
  contactEmail: {
    type: String,
    // required: [true, 'Please add a contact email'],
  },
  youMetUs: {
    type: String,
  },
  role: {
    type: String,
    enum: ['Student', 'Teacher', 'Manager', 'Admin', 'Quality Assurance'],
    default: 'Student',
  },
  email: {
    type: String,
    required: [true, 'Please add an email'],
    unique: true,
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please add a valid email',
    ],
  },
  password: {
    type: String,
    required: [true, 'Please add a password'],
    minlength: 6,
    select: false,
  },
  resetPasswordToken: String,
  resetPasswordExpire: Date,
  verificationCode: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

// encrypt password using
UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) {
    next();
  }

  const passwordEncrypt = await crypto
    .createHash('sha1')
    .update(`${process.env.PASSWORD_ENCRYPT}${this.password}`)
    .digest('hex');

  this.password = passwordEncrypt;
});

// Sign JWT and return
UserSchema.methods.getSignedJwtToken = function () {
  return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRE,
  });
};

// Match user entered password to hashed password in database
UserSchema.methods.matchPassword = async function (enteredPassword) {
  const enteredPasswordEncrypt = await crypto
    .createHash('sha1')
    .update(`${process.env.PASSWORD_ENCRYPT}${enteredPassword}`)
    .digest('hex');
  return this.password == enteredPasswordEncrypt;
};

// Generate and hash verification Code
UserSchema.methods.getVerificationCode = function () {
  const verificationCode = crypto.randomBytes(20).toString('hex');

  this.verificationCode = crypto
    .createHash('sha256')
    .update(verificationCode)
    .digest('hex');

  return verificationCode;
};

module.exports = mongoose.model('User', UserSchema);
