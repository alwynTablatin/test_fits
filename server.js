const express = require('express');
const next = require('next');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const connectDb = require('./config/db');
require('colors');

// middleware
const errorHandler = require('./server/middleware/error');

// next routes
const routes = require('./server/routes/next-routes');

// load env vars
require('dotenv').config({ path: './config/config.env' });

//connectDB
connectDb();

const dev = process.env.NODE_ENV != 'production';

// nextjs config
const nextServer = next({ dev, dir: './client' });
// const nextServer = next({ dev });
// nextjs route hanlder
const handle = routes.getRequestHandler(nextServer);

//route files for backend
const auth = require('./server/routes/auth');

nextServer
  .prepare()
  .then(() => {
    const app = express();

    // body parser
    app.use(express.json());
    // cookie parser
    app.use(cookieParser());
    //Dev logging middleware
    if (process.env.NODE_ENV === 'development') {
      app.use(morgan('dev'));
    }

    // use route
    app.use('/api/v1/auth', auth);

    // user middleware
    app.use(errorHandler);

    app.get('*', (req, res) => {
      return handle(req, res);
    });

    app.listen(process.env.PORT || 5000, (err) => {
      if (err) throw err;

      console.log(
        `server is runnig to port ${process.env.PORT}`.green.underline.bold
      );
    });
  })
  .catch((ex) => {
    console.error(ex);
    process.exit(1);
  });
